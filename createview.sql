CREATE VIEW StudentAdvise AS
SELECT s.StudentID, s.StudentName ,  s.StudentSurname, d.DeptName , a.AdvisorName , a.AdvisorSurname
FROM Student AS s
LEFT JOIN(Department As d , Advisor AS a)
ON (d.DeptID = a.DeptID AND a.AdvisorID = s.AdvisorID);

